import os
import shutil
from multiprocessing import Pool
from typing import Dict, List

import imageio
import matplotlib.pyplot as plt
import networkx as nx
import numpy as np


class Model:
    def __init__(self, grid_size: int):
        self._grid_size = grid_size
        self._graph_size = grid_size ** 2

        self.graph = self._create_graph(grid_size)

        self._graph_reset = False
        self._plots_folder = f'./plots_for_gif_{id(self)}'
        self._plots_template = f'{self._plots_folder}/plot_{{}}.png'

    def _create_graph(self, grid_size):
        graph = nx.generators.grid_2d_graph(grid_size, grid_size, periodic=True)
        mesh = np.array(np.meshgrid([-1, 1], [-1, 1])).reshape((2, -1))
        for node in graph.nodes:
            graph.add_edges_from([(node, tuple(i % self._grid_size)) for i in (mesh + np.array(node).reshape((2, 1))).T])
        return graph

    def reset_graph(self, number_of_agents: List[int]):
        assert all(0 <= number_of_agents_i <= self._graph_size for number_of_agents_i in
                   number_of_agents), "Number of agents is improper"
        assert sum(number_of_agents) <= self._graph_size, "Total number of agents is improper"
        nodes_category = [-1] * (self._graph_size - sum(number_of_agents))
        for i, number_of_agents_i in enumerate(number_of_agents):
            nodes_category += [i] * number_of_agents_i
        np.random.shuffle(nodes_category)
        for node, category in zip(self.graph.nodes, nodes_category):
            self.graph.nodes[node]['category'] = category
            self.graph.nodes[node]['happy'] = None
            self.graph.nodes[node]['happiness_ratio'] = None
        self._graph_reset = True

    def run_simulations(self, to_stay_ratios: List[float], k_neighborhood_radiuses: List[int],
                        max_steps: int = 1000, lonely_agent_happiness: float = 1.):
        self._initialize_simulations()
        for _ in range(max_steps):
            everyone_is_happy = self._update_and_get_happiness(
                to_stay_ratios, k_neighborhood_radiuses, lonely_agent_happiness)
            if everyone_is_happy:
                break
            yield self.graph.copy()
            self._move_unhappy_nodes()
        yield self.graph

    def _initialize_simulations(self):
        if self._graph_reset:
            self._graph_reset = False
        else:
            raise Exception("Graph wasn't reset!")

    def _update_and_get_happiness(self, to_stay_ratios, k_neighborhood_radiuses, lonely_agent_happiness):
        everyone_is_happy = True
        for node, category in self.graph.nodes.data('category'):
            if category == -1:
                self.graph.nodes[node]['happy'] = None
                self.graph.nodes[node]['happiness_ratio'] = None
                continue
            nodes_happiness_ratio = self._calculate_ratio_of_common_neighbors(
                node, category, k_neighborhood_radiuses[category], lonely_agent_happiness
            )
            if nodes_happiness_ratio < to_stay_ratios[category]:
                self.graph.nodes[node]['happy'] = False
                everyone_is_happy = False
            else:
                self.graph.nodes[node]['happy'] = True
        return everyone_is_happy

    def _calculate_ratio_of_common_neighbors(self, node, category, k_neighborhood_radius, lonely_agent_happiness):
        common_neighbors = [
            self.graph.nodes[n]['category'] == category
            for n in nx.single_source_shortest_path(self.graph, node, cutoff=k_neighborhood_radius).keys()
            if self.graph.nodes[n]['category'] != -1 and node != n
        ]
        happiness_ratio = np.mean(common_neighbors) if len(common_neighbors) else lonely_agent_happiness
        self.graph.nodes[node]['happiness_ratio'] = happiness_ratio
        return happiness_ratio

    def _prepare_nodes_data(self):
        free_nodes_data = [node for node, happy in self.graph.nodes.data('happy') if happy is not True]
        np.random.shuffle(free_nodes_data)
        nodes_to_move = [(node, data['category'])
                         for node, data in self.graph.nodes.data(True) if data['happy'] is False]
        np.random.shuffle(nodes_to_move)
        return free_nodes_data, nodes_to_move

    def _move_unhappy_nodes(self):
        free_nodes_data, nodes_to_move = self._prepare_nodes_data()
        for node, category in nodes_to_move:
            for new_node in free_nodes_data:
                if node == new_node:
                    continue
                free_nodes_data.pop(free_nodes_data.index(new_node))
                self.graph.nodes[new_node]['category'] = category
                break
        for new_node in free_nodes_data:
            self.graph.nodes[new_node]['category'] = -1

    def generate_gif_from_trajectory(self, trajectory: List[nx.Graph], node_size: int, colors: Dict[int, str],
                                     filename: str, gif_frame_duration: float = 0.5) -> None:
        os.makedirs(self._plots_folder, exist_ok=True)
        with Pool(20) as p:
            frames = p.starmap(self._create_single_frame,
                               [(i, len(trajectory), g, node_size, colors) for i, g in enumerate(trajectory)])
        imageio.mimwrite(filename, frames, duration=gif_frame_duration)
        shutil.rmtree(self._plots_folder)

    def _create_single_frame(self, step: int, total_steps: int, current_graph: nx.Graph, node_size: int,
                             colors: Dict[int, str]):
        fig = plt.figure(figsize=(10, 10), dpi=100)
        ax = plt.gca()
        nx.draw_networkx_nodes(current_graph,
                               dict(zip(current_graph.nodes, current_graph.nodes)),
                               ax=ax,
                               node_size=node_size,
                               node_color=[colors[i] for _, i in current_graph.nodes.data('category')])
        ax.set_title(f'Step {step + 1}/{total_steps}')
        fig.tight_layout()
        fig.savefig(self._plots_template.format(step))
        plt.close(fig)
        return imageio.imread(self._plots_template.format(step))


if __name__ == '__main__':
    colors = {-1: 'white', 0: 'red', 1: 'blue', 2: 'lime'}
    m = Model(100)
    m.reset_graph([4000, 2500, 1500])
    trajectory = list(m.run_simulations([0.1, 0.5, 0.3], k_neighborhood_radiuses=[3, 1, 2]))
    m.generate_gif_from_trajectory(trajectory, 25, colors, 'additional_3.gif')
