from multiprocessing import Pool


def number_of_iterations_for_n(model, n_list, n_monte_carlo):
    with Pool() as p:
        return dict(p.starmap(monte_carlo_number_of_iterations_for_n,
                              [(model, n, n_monte_carlo) for n in n_list]))


def monte_carlo_number_of_iterations_for_n(model, n, n_monte_carlo):
    number_of_iterations_for_n = 0
    for _ in range(n_monte_carlo):
        model.reset_graph([n, n])
        number_of_iterations_for_n += len(list(model.run_simulations([0.5, 0.5], k_neighborhood_radiuses=[1, 1])))
    number_of_iterations_for_n /= n_monte_carlo
    return n, number_of_iterations_for_n
