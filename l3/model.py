from multiprocessing import Pool
import os
import shutil
from pathlib import Path
from itertools import islice, product
from typing import List

import imageio
import networkx as nx
import numpy as np
import matplotlib.pyplot as plt


class Model:
    def __init__(self, graph_type: str):
        self._graph_type = graph_type

        self._reset_grid = False
        self._history = []
        self._model_filename = None
        self._graph_array = None
        self.graph = None

        self._plots_folder = Path(f'./plots_for_gif_{id(self)}')
        self._plots_template = f'{self._plots_folder}/plot_{{}}.png'

    def generate_grid(self, model_filename: str):
        self._model_filename = model_filename
        self._generate_grid()

    def _generate_grid(self):
        grid_list = Path(self._model_filename).read_text().split()
        self._validate_grid_txt(grid_list)
        m, n = len(grid_list[0]), len(grid_list)
        self.graph: nx.Graph = nx.generators.grid_2d_graph(m, n)
        self._graph_array = np.empty(m * n, dtype=object)
        self._graph_array[:] = list(self.graph.nodes)
        self._graph_array = self._graph_array.reshape((m, n))
        if self._graph_type.lower() in ['lattice', 'cylinder_x', 'cylinder_y', 'torus']:
            self._add_diagonal_edges()
        else:
            raise NotImplemented("Unexpected graph type")
        if self._graph_type.lower() in ['cylinder_x', 'torus']:
            self._add_connections_on_left_and_right_edges()
        if self._graph_type.lower() in ['cylinder_y', 'torus']:
            self._add_connections_on_lower_and_upper_edges()
        for y, row in enumerate(grid_list):
            for x, cell in enumerate(row):
                self.graph.nodes[(x, y)]['alive'] = cell == 'o'
        self._reset_grid = True

    @staticmethod
    def _validate_grid_txt(grid_list):
        error_occurred = False
        first_line_len = len(grid_list[0])
        for i, l in enumerate(grid_list):
            if first_line_len != len(l):
                print(f"Line {i} has different length than first line ({first_line_len} != {len(l)})")
                error_occurred = True
            other_letter = set(l.replace('.', '').replace('o', ''))
            if len(other_letter):
                print(f"On {i}th line unexpected signs were found {other_letter}")
                error_occurred = True
        assert not error_occurred, "Found errors in file"

    def _add_diagonal_edges(self):
        self.graph.add_edges_from(zip(self._graph_array[:-1, :-1].flatten(), self._graph_array[1:, 1:].flatten()))
        self.graph.add_edges_from(zip(self._graph_array[:-1, 1:].flatten(), self._graph_array[1:, :-1].flatten()))

    def _add_connections_on_left_and_right_edges(self):
        self.graph.add_edges_from(zip(self._graph_array[:, 0], self._graph_array[:, -1]))
        self.graph.add_edges_from(zip(self._graph_array[1:, 0], self._graph_array[:-1, -1]))
        self.graph.add_edges_from(zip(self._graph_array[:-1, 0], self._graph_array[1:, -1]))

    def _add_connections_on_lower_and_upper_edges(self):
        self.graph.add_edges_from(zip(self._graph_array[0, :], self._graph_array[-1, :]))
        self.graph.add_edges_from(zip(self._graph_array[0, 1:], self._graph_array[-1, :-1]))
        self.graph.add_edges_from(zip(self._graph_array[0, :-1], self._graph_array[-1, 1:]))

    def run_simulations(self, continue_life_condition: List[int], resurrection_condition: List[int],
                        max_steps: int = 1000):
        if not self._reset_grid:
            self._generate_grid()
        self._reset_grid = False
        self._history = []

        self._add_history()
        for _ in range(max_steps):
            if self._everyone_is_dead():
                break
            previous_graph_state = self.graph.copy()
            yield previous_graph_state
            for node, alive in previous_graph_state.nodes.data('alive'):
                if alive and sum([previous_graph_state.nodes[n]['alive']
                                  for n in previous_graph_state.adj[node]]) not in continue_life_condition:
                    self.graph.nodes[node]['alive'] = False
                elif not alive and sum([previous_graph_state.nodes[n]['alive']
                                        for n in previous_graph_state.adj[node]]) in resurrection_condition:
                    self.graph.nodes[node]['alive'] = True
            self._add_history()
            if self._history_has_loop():
                break
        yield self.graph

    def _add_history(self):
        self._history.append(''.join(str(int(i)) for _, i in self.graph.nodes.data('alive')))

    def _history_has_loop(self):
        for i in range(len(self._history) // 2 + 1):
            if self._history[-i:] == self._history[-2 * i:-i]:
                return True
        return False

    def _everyone_is_dead(self):
        return all(not alive for _, alive in self.graph.nodes.data('alive'))

    def generate_gif_from_trajectory(self, trajectory: List[nx.Graph], node_size: int,
                                     filename: str, gif_frame_duration: float = 0.5) -> None:
        """
        Creates gif from earlier generated (by user) trajectory.
        :param trajectory: list of graphs
        :param node_size: The size of the nodes on the plots
        :param filename: name of the gif file
        :param gif_frame_duration: how long one frame should last in generated gif
        :return:
        """
        os.makedirs(self._plots_folder, exist_ok=True)
        with Pool(20) as p:
            frames = p.starmap(self._create_single_frame,
                               [(i, len(trajectory), g, node_size) for i, g in enumerate(trajectory)])
        imageio.mimwrite(filename, frames, duration=gif_frame_duration)
        shutil.rmtree(self._plots_folder)

    def _create_single_frame(self, step: int, total_steps: int, current_graph: nx.Graph, node_size: int):
        """
        Creates plot of graph with marked S, I, R nodes
        :param step: Number of current step
        :param total_steps: Total number of steps
        :param current_graph: Current graph state
        :return: Image of the generated plot
        """
        colors = ['whitesmoke', 'black']
        fig = plt.figure(figsize=(10, 10), dpi=200)
        ax = plt.gca()
        nx.draw_networkx_nodes(current_graph,
                               dict(zip(current_graph.nodes, current_graph.nodes)),
                               ax=ax,
                               node_size=node_size,
                               node_color=[colors[int(i)] for _, i in current_graph.nodes.data('alive')])
        ax.set_title(f'Step {step + 1}/{total_steps}')
        fig.tight_layout()
        fig.savefig(self._plots_template.format(step))
        plt.close(fig)
        return imageio.imread(self._plots_template.format(step))


if __name__ == '__main__':
    m = Model('lattice')
    m.generate_grid('test_structure.txt')
    list(m.run_simulations([2, 3], [3]))
    print(1)

