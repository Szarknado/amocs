from itertools import product
import os
import shutil
from typing import Optional, Tuple

import imageio
import matplotlib.pyplot as plt
import numpy as np
from scipy import spatial


class Boid:
    def __init__(self, pos: Optional[np.ndarray] = None, vel: Optional[np.ndarray] = None,
                 radius_of_vision: float = 10., safety_radius: float = 1.,
                 angle_of_vision: float = 2 / 3 * np.pi, color: str = 'black'):
        assert 0 < angle_of_vision < np.pi, "The angle of vision must be between 0 and pi (180 degrees)"
        self.pos = pos if pos is not None else np.random.normal(loc=5, size=2)
        self.vel = vel if vel is not None else np.random.normal(scale=0.1, size=2)
        self.radius_of_vision = radius_of_vision
        self.angle_of_vision = angle_of_vision
        self.safety_radius = safety_radius
        self.color = color

        self.cos_of_angle = np.cos(angle_of_vision)

    def is_in_field_of_vision(self, other: 'Boid') -> bool:
        return spatial.distance.euclidean(self.pos, other.pos) < self.radius_of_vision and \
               (True if self.vel.sum() < 1e-3 else (1 - spatial.distance.cosine(self.vel,
                                                                                other.pos - self.pos)) > self.cos_of_angle)

    def is_in_safety_area(self, other: 'Boid') -> bool:
        return spatial.distance.euclidean(self.pos, other.pos) < self.safety_radius


class BoidsModel:
    def __init__(self, max_speed, coherency_influence, alignment_influence, separation_influence,
                 separation_division_margin: float = 1e-3,
                 bounds: Tuple[float, float, float, float] = (0., 10., 0., 10.)):
        self.max_speed = max_speed
        self.coherency_influence = coherency_influence
        self.alignment_influence = alignment_influence
        self.separation_influence = separation_influence
        self.separation_division_margin = separation_division_margin
        self.bounds = np.array(bounds).reshape(-1, 2)

        self.boids = None
        self._plots_folder = f'./plots_for_gif_{id(self)}'
        self._plots_template = f'{self._plots_folder}/plot_{{}}.png'

    def __del__(self):
        shutil.rmtree(self._plots_folder)

    def run(self, gif_filename: str, n_steps: int = 100) -> None:
        if self.boids is None:
            print("No boids provided. Generating 100 random boids")
            self.boids = [Boid() for _ in range(100)]
        self._create_single_frame(0, n_steps)
        for n in range(1, n_steps + 1):
            all_steps = {b: self.boid_step(b) for b in self.boids}
            for b, (new_pos, new_vel) in all_steps.items():
                b.pos = self._convert_to_periodic_boundary(new_pos, self.bounds.mean(axis=1))[0]
                b.vel = new_vel
            self._create_single_frame(n, n_steps)
        self.generate_gif_from_trajectory(gif_filename, n_steps)

    def boid_step(self, boid: Boid) -> Tuple[np.ndarray, np.ndarray]:
        other_boids_in_vision = [b for b in self.boids if b != boid and boid.is_in_field_of_vision(b)]
        other_boids_in_safety_area = [b for b in self.boids if b != boid and boid.is_in_safety_area(b)]
        # coherency
        coherency_caused_speed = np.mean(
            [self._convert_to_periodic_boundary(nboid.pos, boid.pos)[0] - boid.pos for nboid in other_boids_in_vision],
            axis=0
        ) if other_boids_in_vision else 0.
        # alignment
        alignment_caused_speed = np.mean([x.vel for x in other_boids_in_vision], axis=0) \
            if other_boids_in_vision else 0.
        # separation
        separation_caused_speed = np.mean(
            [self._get_separation_speed_vector(nboid.pos, boid.pos) for nboid in other_boids_in_safety_area],
            axis=0) if other_boids_in_safety_area else 0.
        new_vel = boid.vel + \
                  self.coherency_influence * coherency_caused_speed + \
                  self.alignment_influence * alignment_caused_speed + \
                  self.separation_influence * separation_caused_speed
        new_vel = self._adjust_speed(new_vel)
        new_pos = boid.pos + new_vel
        return new_pos, new_vel

    def _convert_to_periodic_boundary(self, nboid_pos: np.ndarray, boid_pos: np.ndarray) -> Tuple[np.ndarray, float]:
        distances = [(nboid_pos.copy(), np.linalg.norm(nboid_pos - boid_pos))]

        for i, j in product(range(2), range(2)):
            nboid_pos_tmp = nboid_pos.copy()
            nboid_pos_tmp[i] = self.bounds[i][j] + nboid_pos_tmp[i] - self.bounds[i][1 - j]
            distances.append((nboid_pos_tmp.copy(), np.linalg.norm(nboid_pos_tmp - boid_pos)))

        return min(distances, key=lambda x: x[1])

    def _get_separation_speed_vector(self, nboid_pos: np.ndarray, boid_pos: np.ndarray):
        new_nboid_pos, boids_norm = self._convert_to_periodic_boundary(nboid_pos, boid_pos)
        return (new_nboid_pos - boid_pos) / (boids_norm + self.separation_division_margin) ** 2

    def _adjust_speed(self, vel: np.ndarray) -> np.ndarray:
        vel_norm = np.linalg.norm(vel)
        return vel if vel_norm <= self.max_speed else vel / vel_norm * self.max_speed

    def _create_single_frame(self, step: int, total_steps: int) -> None:
        os.makedirs(self._plots_folder, exist_ok=True)
        fig = plt.figure(figsize=(10, 10), dpi=100)
        ax = plt.gca()
        for b in self.boids:
            plt.plot(*b.pos, marker=(3, 0, np.degrees(np.arctan2(-b.vel[0], b.vel[1]))),
                     markersize=20, linestyle='None', color=b.color)
            plt.plot(*list(zip(b.pos, b.pos + b.vel)), color='r')
        ax.set_title(f'Step {step}/{total_steps}')
        ax.set_xlim(*self.bounds[0])
        ax.set_ylim(*self.bounds[1])
        fig.tight_layout()
        fig.savefig(self._plots_template.format(step))
        plt.close(fig)

    def generate_gif_from_trajectory(self, filename: str, total_steps: int, gif_frame_duration: float = 0.5) -> None:
        frames = [imageio.imread(self._plots_template.format(step)) for step in range(total_steps + 1)]
        imageio.mimwrite(filename, frames, duration=gif_frame_duration)


if __name__ == '__main__':
    bm = BoidsModel(1., 1., 1., 1.)
    bm.run("example.gif", 20)
