from typing import List, Tuple

from nn_influence_group_model import NNInfluenceGroupModel


def calculate_magnetization_n_times(p: float, f: float, q: int, number_of_runs: int, time_steps: int,
                                    nn_model: NNInfluenceGroupModel) -> Tuple[float, float, List[List[float]]]:
    """
    Function that generates trajectories `number_of_runs` times
    :param p: model's parameter - p
    :param f: model's parameter - f
    :param q: model's parameter - q
    :param number_of_runs: number of runs
    :param time_steps: model's parameter - number of time steps
    :param nn_model: instance of NNInfluenceGroupModel
    :return:
    """
    magnetization_in_time_tmp = []
    for _ in range(number_of_runs):
        magnetization_in_time_tmp.append(nn_model.generate_trajectory(p, f, q, time_steps))
    return p, f, magnetization_in_time_tmp
