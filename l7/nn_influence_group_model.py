from itertools import cycle
from random import choices, sample
from typing import List, Optional, Union

import numpy as np
import networkx as nx
from networkx import Graph, DiGraph


class NNInfluenceGroupModel:
    def __init__(self, graph: Union[Graph, DiGraph]):
        """
        :param graph: Graph on which we want to perform random walk
        """
        self._graph: DiGraph = graph if isinstance(graph, DiGraph) else graph.to_directed()
        nx.set_node_attributes(self._graph, 1, 'spin')
        self._magnetization_in_time = None

    @property
    def magnetization_in_time(self) -> Optional[List[float]]:
        """
        :return: generated magnetization in time. If trajectory wasn't generated returns None.
        """
        return self._magnetization_in_time

    @property
    def graph(self) -> DiGraph:
        """
        :return: graph as directed graph
        """
        return self._graph

    def generate_trajectory(self, p: float, f: float, q: int, monte_carlo_steps: int, epsilon: float = 0.)\
            -> List[float]:
        """
        Generates single trajectory of the model. Returns magnetization in time
        :param p: independence factor (0 <= p <= 1)
        :param f: probability if agent flips opinion (0 <= p <= 1)
        :param q: number of randomly picked neighbors - positive int
        :param monte_carlo_steps: number of time-steps
        :param epsilon: the probability of spin flip if spinson does not have an unanimous opinion
        :return: trajectory
        """
        if not 0 <= p <= 1:
            raise ValueError("Probability p is out of range")
        if not 0 <= f <= 1:
            raise ValueError("Probability f is out of range")
        if not 0 <= epsilon <= 1:
            raise ValueError("Probability epsilon is out of range")
        if q < 0:
            raise ValueError("Number of neighbors can't be negative.")
        current_graph = self._graph.copy()
        N = current_graph.number_of_nodes()
        nodes_neighbors = {node: list(dict(current_graph[node]).keys()) for node in current_graph}

        self._magnetization_in_time = [self.get_magnetization(current_graph)]

        for (n, (rand_1, rand_2), picked_spinson) in zip(cycle(range(1, N+1)),
                                                         np.random.random((monte_carlo_steps * N, 2)),
                                                         choices(list(current_graph.nodes()), k=monte_carlo_steps * N)):
            if rand_1 < p:
                if rand_2 < f:
                    current_graph.nodes[picked_spinson]['spin'] *= -1
            elif len(nodes_neighbors[picked_spinson]) >= q:
                neighbors_spins = [current_graph.nodes[spinson]['spin']
                                   for spinson in sample(nodes_neighbors[picked_spinson], k=q)]
                if len(set(neighbors_spins)) == 1:
                    current_graph.nodes[picked_spinson]['spin'] = neighbors_spins[0]
                elif rand_2 < epsilon:
                    current_graph.nodes[picked_spinson]['spin'] *= -1
            if n == N:
                self._magnetization_in_time.append(self.get_magnetization(current_graph))
        return self._magnetization_in_time

    @staticmethod
    def get_magnetization(current_graph: DiGraph) -> float:
        """
        Calculates the magnetization of a given graph.
        :param current_graph:
        :return: mean magnetization
        """
        return NNInfluenceGroupModel._average_from_list(list(dict(current_graph.nodes(data='spin')).values()))

    @staticmethod
    def _average_from_list(list_of_values: List[float]) -> float:
        """
        Calculates the average value from list of values
        :param list_of_values: list of values
        :return: mean value
        """
        return sum(list_of_values) / len(list_of_values)


if __name__ == '__main__':
    graph = NNInfluenceGroupModel(nx.barabasi_albert_graph(100, 4))
    print(graph.generate_trajectory(0.5, 0.5, 2, 1000))
