from multiprocessing import Pool
import os
import shutil
from itertools import islice, product
from typing import List

import imageio
import networkx as nx
import numpy as np
import matplotlib.pyplot as plt


class ForestFireModel:
    def __init__(self, grid_size: int):
        self._grid_size = grid_size

        self.graph = nx.generators.grid_2d_graph(grid_size, grid_size)

        self._graph_reset = False
        self._plots_folder = f'./plots_for_gif_{id(self)}'
        self._plots_template = f'{self._plots_folder}/plot_{{}}.png'

    def reset_graph(self, probability_of_planting_tree: float = 0.5):
        assert 0 <= probability_of_planting_tree <= 1, "Probability is out of range"
        for node, prob in zip(self.graph.nodes, np.random.random(size=len(self.graph.nodes))):
            self.graph.nodes[node]['state'] = int(prob <= probability_of_planting_tree)
        self._graph_reset = True

    def run_simulations(self):
        yield from self._initialize_simulations()

        while self._trees_are_burning():
            previous_graph_state = self.graph.copy()
            yield previous_graph_state
            for node, state in previous_graph_state.nodes.data('state'):
                neighbors = [(i + node[0], j + node[1]) for i, j in product(*[[-1, 0, 1]]*2)]
                if state == 1 and any(previous_graph_state.nodes[i]['state'] == 2 for i in neighbors
                                      if i in previous_graph_state.nodes and i != node):
                    self.graph.nodes[node]['state'] = 2
                elif state == 2:
                    self.graph.nodes[node]['state'] = 3
        yield self.graph

    def _initialize_simulations(self):
        if self._graph_reset:
            self._graph_reset = False
        else:
            raise Exception("Graph wasn't reset!")
        yield self.graph.copy()
        self._start_fire()

    def _start_fire(self):
        for node, state in islice(self.graph.nodes.data('state'), self._grid_size):
            if state == 1:
                self.graph.nodes[node]['state'] = 2

    def _trees_are_burning(self):
        return any(state == 2 for _, state in self.graph.nodes.data('state'))

    def calculate_ellipse_matrix(self, force):
        x_0, y_0, a, b, phi = self.translate_force_to_ellipse_parameters(force)
        ellipse_matrix = {node: [other_node for other_node in self.graph.nodes
                                 if node != other_node and self.is_inside_ellipse(*other_node, x_0 + node[0], y_0 + node[1], a, b, phi)]
                          for node in self.graph.nodes}
        return ellipse_matrix

    def run_simulations_with_wind(self, ellipse_matrix):
        yield from self._initialize_simulations()

        while self._trees_are_burning():
            previous_graph_state = self.graph.copy()
            yield previous_graph_state
            for node_on_fire, state in previous_graph_state.nodes.data('state'):
                if state == 2:
                    self.graph.nodes[node_on_fire]['state'] = 3
                    for node_feeling_fire in ellipse_matrix[node_on_fire]:
                        if self.graph.nodes[node_feeling_fire]['state'] == 1:
                            self.graph.nodes[node_feeling_fire]['state'] = 2
        yield self.graph

    @staticmethod
    def translate_force_to_ellipse_parameters(force, free_space=1.5):
        r = 1 + np.linalg.norm(force)
        angle = np.arctan2(force[1], force[0])
        a = (r + free_space) / 2
        return (a - free_space) * np.cos(angle), (a - free_space) * np.sin(angle), a, 1, angle

    @staticmethod
    def is_inside_ellipse(x, y, x_0, y_0, a, b, phi):
        return np.power(((x - x_0) * np.cos(phi) + (y - y_0) * np.sin(phi)) / a, 2) + \
               np.power(((x - x_0) * np.sin(phi) - (y - y_0) * np.cos(phi)) / b, 2) < 1.001

    def generate_gif_from_trajectory(self, trajectory: List[nx.Graph], node_size: int,
                                     file_name: str, gif_frame_duration: float = 0.5) -> None:
        """
        Creates gif from earlier generated (by user) trajectory.
        If trajectory wasn't generated error is raised.
        :param trajectory: list of graphs
        :param node_size: The size of the nodes on the plots
        :param file_name: name of the gif file
        :param gif_frame_duration: how long one frame should last in generated gif
        :return:
        """
        if trajectory is None:
            raise ValueError("Trajectory wasn't generated. Use generate_trajectory before generating gif.")
        elif len(trajectory) < 2:
            print(f"Trajectory is to short to generate gif! It has length {len(trajectory)}")
            return
        os.makedirs(self._plots_folder, exist_ok=True)
        with Pool(20) as p:
            frames = p.starmap(self._create_single_frame,
                               [(i, len(trajectory), g, node_size) for i, g in enumerate(trajectory)])
        imageio.mimwrite(file_name, frames, duration=gif_frame_duration)
        shutil.rmtree(self._plots_folder)

    def _create_single_frame(self, step: int, total_steps: int, current_graph: nx.Graph, node_size: int):
        """
        Creates plot of graph with marked S, I, R nodes
        :param step: Number of current step
        :param total_steps: Total number of steps
        :param current_graph: Current graph state
        :return: Image of the generated plot
        """
        colors = ['white', 'green', 'red', 'black']
        fig = plt.figure(figsize=(10, 10), dpi=50)
        ax = plt.gca()
        nx.draw_networkx_nodes(current_graph, dict(zip(current_graph.nodes, current_graph.nodes)),
                               ax=ax,
                               node_size=node_size,
                               node_color=[colors[i] for _, i in current_graph.nodes.data('state')])
        ax.set_title(f'Tree model - step {step + 1}/{total_steps}')
        fig.tight_layout()
        fig.savefig(self._plots_template.format(step))
        plt.close(fig)
        return imageio.imread(self._plots_template.format(step))


if __name__ == '__main__':
    ffm = ForestFireModel(10)
    ffm.reset_graph()
    [3, 2]
    list(ffm.run_simulations_with_wind())
