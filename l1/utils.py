from multiprocessing import Pool

import networkx as nx
import numpy as np

from l1.forest_fire_model import ForestFireModel


def is_opposite(p: float, n: int, force=None):
    model = ForestFireModel(n)
    model.reset_graph(p)
    list_of_serched_nodes = [(n-1, y_pos) for y_pos in range(n)]
    for _ in model.run_simulations():
        continue
    for node in list_of_serched_nodes:
        if model.graph.nodes.data('state')[node] == 3:
            return 1
    return 0


def is_opposite_wind(p: float, n: int, force):
    model = ForestFireModel(n)
    model.reset_graph(p)
    list_of_serched_nodes = [(n-1, y_pos) for y_pos in range(n)]
    ellipse_matrix = model.calculate_ellipse_matrix(force=force)
    for _ in model.run_simulations_with_wind(ellipse_matrix):
        continue
    for node in list_of_serched_nodes:
        if model.graph.nodes.data('state')[node] == 3:
            return 1
    return 0


def cluster_size(p: float, n: int, force=None):
    model = ForestFireModel(n)
    model.reset_graph(p)
    for _ in model.run_simulations():
        continue
    x = model.graph.copy()
    nodes_to_remove = [node for node, state in x.nodes.data('state') if state != 3]
    x.remove_nodes_from(nodes_to_remove)
    try:
        return len(max(nx.connected_components(x), key=len))
    except ValueError:
        return 0


def cluster_size_wind(p: float, n: int, force):
    model = ForestFireModel(n)
    model.reset_graph(p)
    ellipse_matrix = model.calculate_ellipse_matrix(force=force)
    for _ in model.run_simulations_with_wind(ellipse_matrix):
        continue
    x = model.graph.copy()
    nodes_to_remove = [node for node, state in x.nodes.data('state') if state != 3]
    x.remove_nodes_from(nodes_to_remove)
    try:
        return len(max(nx.connected_components(x), key=len))
    except ValueError:
        return 0


def monte_carlo(func, p=.5, n=10, force=None, repeat=50):
    return np.sum([func(p, n, force) for _ in range(repeat)])/repeat


def pooling_monte_carlo(f, sizes: np.ndarray, probs: np.ndarray, force=None):
    probs_dict = {}
    for n in sizes:
        probs_dict[n] = Pool().starmap(monte_carlo, [(f, prob, n, force) for prob in probs])
    return probs_dict
