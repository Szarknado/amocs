from itertools import cycle
from multiprocessing.pool import ThreadPool
import os
from random import random, sample
import shutil
from typing import Any, Optional, List, Union

import imageio
import numpy as np
import networkx as nx
from networkx import Graph, DiGraph
import matplotlib.pyplot as plt


class Model:
    def __init__(self, graph: Union[Graph, DiGraph]):
        self._graph: DiGraph = graph if isinstance(graph, DiGraph) else graph.to_directed()
        nx.set_node_attributes(self._graph, False, 'state')
        nx.set_node_attributes(self._graph, 'imitator', 'society')
        self._trajectory = None
        self._plots_folder = f'./plots_for_gif_{id(self)}'
        self._plots_template = self._plots_folder + '/plot_{}.png'

    @property
    def trajectory(self) -> Optional[List[DiGraph]]:
        return self._trajectory

    @property
    def graph(self) -> DiGraph:
        return self._graph

    def generate_trajectory(self,
                            probability_of_influence: float,
                            probability_of_innovation: float,
                            percent_of_innovators: float,
                            steps: Optional[int] = None) -> List:
        current_graph = self._graph.copy()
        for node in sample(self._graph.nodes, int(self._graph.number_of_nodes() * percent_of_innovators)):
            current_graph.nodes[node]['society'] = 'innovator'
        self._trajectory = [current_graph]
        for _ in range(steps) if steps else cycle(range(1)):
            not_adopted_nodes = self._get_nodes_in_given_state(current_graph, False)
            if steps is None and len(not_adopted_nodes) == 0:
                return self._trajectory
            current_graph = self._simulate_one_time_step(
                current_graph, not_adopted_nodes, probability_of_influence, probability_of_innovation)
            self._trajectory.append(current_graph)
        return self._trajectory

    @staticmethod
    def _get_nodes_in_given_state(current_graph: DiGraph, state: Any, state_variable_name: str = 'state') -> List:
        return [node for node, node_state in dict(current_graph.nodes(data=state_variable_name)).items()
                if node_state == state]

    def _simulate_one_time_step(self, old_graph: DiGraph, not_adopted_nodes: List,
                                probability_of_influence: float, probability_of_innovation: float) \
            -> DiGraph:
        current_graph = old_graph.copy()
        for node in not_adopted_nodes:
            adjacent_adopters = [n for n in current_graph.adj[node] if old_graph.nodes[n]['state']]
            if current_graph.nodes[node]['society'] == 'innovator':
                current_graph.nodes[node]['state'] = random() < probability_of_innovation
            elif not len(adjacent_adopters) == 0:
                prob_vector = np.random.random(len(adjacent_adopters))
                current_graph.nodes[node]['state'] = any(prob_vector <= probability_of_influence)
        return current_graph

    def generate_gif_from_trajectory(self, gif_frame_duration: float = 0.5, file_name: str = 'graph_sir.gif',
                                     nodes_indicate_position: bool = False) -> None:
        self._nodes_indicate_position = nodes_indicate_position
        if self._trajectory is None:
            raise ValueError("Trajectory wasn't generated. Use generate_trajectory before generating gif.")
        elif len(self._trajectory) < 2:
            print(f"Trajectory is to short to generate gif! It has length {len(self._trajectory)}")
            return
        os.makedirs(self._plots_folder, exist_ok=True)
        with ThreadPool(20) as p:
            frames = p.starmap(self._create_single_frame, enumerate(self._trajectory))
        imageio.mimwrite(file_name, frames, duration=gif_frame_duration)
        shutil.rmtree(self._plots_folder)

    def _create_single_frame(self, step: int, current_graph: DiGraph):
        fig = plt.figure(figsize=(10, 10))
        ax = plt.gca()
        if self._nodes_indicate_position:
            pos = dict(zip(self._graph.nodes, self._graph.nodes))
        else:
            pos = nx.circular_layout(self._graph)

        nx.draw_networkx(current_graph, pos=pos, ax=ax, with_labels=False)

        not_adopted_nodes = self._get_nodes_in_given_state(current_graph, False)
        adopted_nodes = self._get_nodes_in_given_state(current_graph, True)

        nx.draw_networkx_nodes(current_graph, pos=pos, ax=ax, nodelist=not_adopted_nodes,
                               node_color='black', node_size=500, label='Not Adopted')
        nx.draw_networkx_nodes(current_graph, pos=pos, ax=ax, nodelist=adopted_nodes,
                               node_color='red', node_size=300, label='Adopted')

        ax.set_title(f'Model step {step + 1}/{len(self._trajectory)}' + '\n'
                                                                        f'Adopted ({len(adopted_nodes)}) / not adopted ({len(not_adopted_nodes)})')
        ax.legend()
        fig.tight_layout()
        fig.savefig(self._plots_template.format(step))
        plt.close(fig)
        return imageio.imread(self._plots_template.format(step))

    def get_fraction_of_nodes_in_sate(self, state: str, state_variable_name: str = 'state') -> List[float]:
        if self._trajectory is None:
            raise ValueError("Trajectory wasn't generated. Use generate_trajectory before generating gif.")
        total_number_of_nodes = self._graph.number_of_nodes()
        return [len(self._get_nodes_in_given_state(current_graph, state, state_variable_name)) / total_number_of_nodes
                for current_graph in self._trajectory]


if __name__ == '__main__':
    model = Model(nx.grid_graph([20, 20]))
    model.generate_trajectory(0.05, 0.1, 0.3)
    model.generate_gif_from_trajectory(file_name='realization.gif', nodes_indicate_position=True)
