import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns


class TrafficSimulation:

    def __init__(self, road_length=100, density=0.2, max_velocity=5, slow_prob=0.3, v0=None):
        self.road_length = road_length
        self.density = density
        self.max_velocity = max_velocity
        self.slow_prob = slow_prob
        self.average_velocity = []

        self.state = -np.ones(road_length, dtype=int)
        for i in range(road_length):
            if np.random.rand() <= self.density:
                if v0:
                    self.state[i] = v0
                else:
                    self.state[i] = np.random.randint(0, max_velocity)

    def update(self):
        next_state = -np.ones(self.road_length, dtype=int)

        current_index = np.argwhere(self.state != -1)
        num_cars = len(current_index)

        for i in range(num_cars):
            c = current_index[i]
            n = current_index[(i + 1) % len(current_index)]
            velocity = self.state[c]

            dist = (n - c) % self.road_length

            if velocity < self.max_velocity and dist >= velocity + 1:
                velocity = velocity + 1

            if velocity > 0 and dist <= velocity:
                velocity = dist - 1

            if velocity > 0 and np.random.rand() <= self.slow_prob:
                velocity = velocity - 1

            next_state[(c + velocity) % self.road_length] = velocity

            self.average_velocity.append(np.mean(next_state[np.argwhere(next_state != -1)]))
        self.state = next_state

    def simulation(self, time_steps=1000):
        matrix = [self.state]
        for _ in range(time_steps):
            self.update()
            matrix.append(self.state)

        return matrix, self.average_velocity


if __name__ == '__main__':
    flag = 3
    if flag == 1:
        densities = [0.1, 0.2, 0.6]
        for rho in densities:
            model = TrafficSimulation(density=rho, road_length=500, max_velocity=5, slow_prob=0.3)
            mat, avg = model.simulation()
            figure = plt.figure(figsize=(16, 8))
            for t, x in enumerate(mat):
                plt.scatter(np.argwhere(x != -1), [t] * len(np.argwhere(x != -1)), color='k', s=1, alpha=0.1)
            plt.xlabel(u'vehicle location')
            plt.ylabel(u'time')
            plt.title(r'Traffic simulation for cars density $%.1f$' % rho)
            figure.savefig(f"Evolution_of_the_system_rho_{rho}.png")

    if flag == 2:
        densities = np.linspace(0.05, 1, 50)

        probabilities = [0, 0.3, 0.5]
        colors = ['r', 'b', 'g']
        sns.set()

        figure = plt.figure(figsize=(16, 8))
        for i, p in enumerate(probabilities):
            li = []
            print(f"Probability:{p}")
            for rho in densities:
                print(rho)
                li1 = []
                for _ in range(50):
                    model = TrafficSimulation(density=rho, road_length=200, max_velocity=5, slow_prob=p)
                    mat, avg = model.simulation()
                    li1.append(np.mean(avg))
                li.append(np.mean(li1))
            np.savetxt(f'One_Lines_Averages_for_{p}.txt', np.array(li), newline=" ")
            plt.plot(densities, li, color=colors[i], label=f'p={p}')
        plt.xlabel("Car density")
        plt.ylabel("Average velocity")
        plt.title("Dependence between average velocity and cars density")
        plt.legend()
        figure.savefig("Average_Density.png")

    if flag == 3:
        probabilities = [0, 0.3, 0.5]
        densities = np.linspace(0.05, 1, 50)

        for prob in probabilities:
            sns.set()
            fig = plt.figure(figsize=(16, 8))
            plt.plot(densities, np.loadtxt(f"One_Lines_Averages_for_{prob}.txt"), label="One line")
            plt.plot(densities, np.loadtxt(f"Two_Lines_Averages_for_{prob}.txt"), label="Two lines")
            plt.xlabel("Car density")
            plt.ylabel("Average velocity")
            plt.title(f"Compare two models for probability {prob}")
            plt.legend()
            fig.savefig(f"Compare_probability_{prob}.png")


