import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt


class TrafficSimulationTwoLanes:

    def __init__(self, road_length=100, density=0.2, max_velocity=5, slow_prob=0.3):
        self.road_length = road_length
        self.density = density
        self.max_velocity = max_velocity
        self.slow_prob = slow_prob
        self.average_velocity = []

        self.state1 = -np.ones(road_length, dtype=int)
        self.state2 = -np.ones(road_length, dtype=int)

        for i in range(road_length):
            if np.random.rand() <= self.density:
                self.state1[i] = np.random.randint(0, max_velocity)
            if np.random.rand() <= self.density:
                self.state2[i] = np.random.randint(0, max_velocity)

    def update(self):

        next_state1 = -np.ones(self.road_length, dtype=int)
        next_state2 = -np.ones(self.road_length, dtype=int)

        current_index1 = np.argwhere(self.state1 != -1)
        current_index2 = np.argwhere(self.state2 != -1)
        update_index1 = current_index1
        update_index2 = current_index2

        for i in range(len(current_index1)):
            c = current_index1[i]
            n1 = current_index1[(i + 1) % len(current_index1)]
            velocity = self.state1[c]

            if (n1 - c) % self.road_length - 1 >= velocity + 1: break

            if self.state2[c] >= 0 or (n1 - c) % self.road_length > velocity: break

            for dist in range(1, self.max_velocity):
                if self.state2[(c - dist) % self.road_length] >= 0:
                    look_back = False
                else:
                    look_back = True

            for dist in range(1, self.max_velocity):
                if self.state2[(c + dist) % self.road_length] >= 0:
                    look_ahead = dist - 1
                    break
                else:
                    look_ahead = self.max_velocity - 1

            if look_back and look_ahead >= velocity:

                c_index = np.argwhere(update_index1 == c)

                new_index1 = np.delete(update_index1, c_index)

                update_index1 = new_index1

                update_index2 = update_index2.flatten()

                insertindex = np.searchsorted(update_index2, c[0])

                update_index2 = np.insert(update_index2, insertindex, c)

        for i in range(len(current_index2)):
            c = current_index2[i]
            n2 = current_index2[(i + 1) % len(current_index2)]
            velocity = self.state2[c]

            if (n2 - c) % self.road_length >= velocity + 1: break

            if self.state1[c] >= 0: break

            for dist in range(1, self.max_velocity):
                if self.state1[(c - dist) % self.road_length] >= 0:
                    look_back = False
                else:
                    look_back = True

            for dist in range(1, self.max_velocity):
                if self.state1[(c + dist) % self.road_length] >= 0:
                    look_ahead = dist - 1
                    break
                else:
                    look_ahead = self.max_velocity - 1

            if look_back == True and look_ahead >= velocity:

                c_index = np.argwhere(update_index2 == c)

                new_index2 = np.delete(update_index2, c_index)

                update_index2 = new_index2

                update_index1 = update_index1.flatten()

                insertindex = np.searchsorted(update_index1, c[0])

                update_index1 = np.insert(update_index1, insertindex, c)

        for i in range(len(update_index1)):

            c = update_index1[i]
            n = update_index1[(i + 1) % len(update_index1)]
            velocity = self.state1[c]

            if velocity < self.max_velocity and (n - c) % self.road_length > velocity:
                velocity = velocity + 1

            if velocity > 0 and (n - c) % self.road_length <= velocity:
                velocity = (n - c) % self.road_length - 1

            if velocity > 0 and np.random.rand() <= self.slow_prob:
                velocity = velocity - 1

            next_state1[(c + velocity) % self.road_length] = velocity

        self.average_velocity.append(np.mean(velocity))

        for i in range(len(update_index2)):

            c = update_index2[i]
            n = update_index2[(i + 1) % len(update_index2)]
            velocity = self.state2[c]

            if velocity < self.max_velocity and (n - c) % self.road_length > velocity:
                velocity = velocity + 1

            if velocity > 0 and (n - c) % self.road_length <= velocity:
                velocity = (n - c) % self.road_length - 1

            if velocity > 0 and np.random.rand() <= self.slow_prob:
                velocity = velocity - 1

            next_state2[(c + velocity) % self.road_length] = velocity

        self.average_velocity.append(np.mean(velocity))

        self.state1 = next_state1
        self.state2 = next_state2

    def simulation(self, time_steps=1000):
        matrix1 = [self.state1]
        matrix2 = [self.state2]
        for _ in range(time_steps):
            self.update()
            matrix1.append(self.state1)
            matrix2.append(self.state2)
        return matrix1, matrix2, self.average_velocity


if __name__ == '__main__':
    densities = np.linspace(0.05, 1, 50)

    probabilities = [0, 0.3, 0.5]
    colors = ['r', 'b', 'g']
    sns.set()

    figure = plt.figure(figsize=(16, 8))
    for i, p in enumerate(probabilities):
        li = []
        print(f"Probability:{p}")
        for rho in densities:
            print(rho)
            li1 = []
            for _ in range(50):
                model = TrafficSimulationTwoLanes(density=rho, road_length=200, max_velocity=5, slow_prob=p)
                mat1, mat2, avg = model.simulation()
                li1.append(np.mean(avg))
            li.append(np.mean(li1))
        np.savetxt(f'Two_Lines_Averages_for_{p}.txt', np.array(li), newline=" ")
        plt.plot(densities, li, color=colors[i], label=f'p={p}')

    plt.xlabel("Car density")
    plt.ylabel("Average velocity")
    plt.title("Dependence between average velocity and cars density in two lanes road")
    plt.legend()
    figure.savefig("Average_Density_Two_Lanes.png")